<?php
function distance($lat1, $long1, $lat2, $long2) {
$a = 6378137.0; # equatorial radius in meters
$f = 1/298.257223563; # ellipsoid flattening
$b = (1 - $f)*$a;
$tolerance = 1e-11; # to stop iteration

$phi1 = deg2rad($lat1);
$phi2 = deg2rad($lat2);
$U1 = atan((1-$f)*tan($phi1));
$U2 = atan((1-$f)*tan($phi2));
// L1, L2 = long1, long2
$L1 = deg2rad($long1);
$L2 = deg2rad($long2);
$L = $L2 - $L1;

$lambda_old = $L + 0;

while(1){
    $t = pow((cos($U2)*sin($lambda_old)),2);
    $t += pow((cos($U1)*sin($U2) - sin($U1)*cos($U2)*cos($lambda_old)),2);
    $sin_sigma = pow($t,0.5);
    $cos_sigma= sin($U1)*sin($U2) + cos($U1)*cos($U2)*cos($lambda_old);
    $sigma = atan2($sin_sigma, $cos_sigma);

    $sin_alpha = cos($U1)*cos($U2)*sin($lambda_old) / $sin_sigma;
    $cos_sq_alpha = 1 - pow($sin_alpha,2);
    $cos_2sigma_m =$cos_sigma- 2*sin($U1)*sin($U2)/$cos_sq_alpha;
    $C = $f*$cos_sq_alpha*(4 + $f*(4-3*$cos_sq_alpha))/16;

    $t = $sigma + $C*$sin_sigma*($cos_2sigma_m + $C*$cos_sigma*(-1 + 2*pow($cos_2sigma_m,2)));
    $lambda_new = $L + (1 - $C)*$f*$sin_alpha*$t;
    if (abs($lambda_new - $lambda_old) <= $tolerance)
        break;
    else $lambda_old = $lambda_new;
}

$u2 = $cos_sq_alpha*((pow($a,2) - pow($b,2))/pow($b,2));
$A = 1 + ($u2/16384)*(4096 + $u2*(-768+$u2*(320 - 175*$u2)));
$B = ($u2/1024)*(256 + $u2*(-128 + $u2*(74 - 47*$u2)));
$t = $cos_2sigma_m + 0.25*$B*($cos_sigma*(-1 + 2*pow($cos_2sigma_m,2)));
$t -= ($B/6)*$cos_2sigma_m*(-3 + 4*pow($sin_sigma,2))*(-3 + 4*pow($cos_2sigma_m,2));
$delta_sigma = $B * $sin_sigma * $t;
$s = $b*$A*($sigma - $delta_sigma);
return $s;
}
echo distance(20.9977344, 105.7947648, 20.998029, 105.7946391);
